---
title: Sobre
---

# Sobre o CCOS

O **Centro de Competência em Open Source (CCOS)** é um órgão do [Instituto de
Ciências Matemáticas e de Computação](https://icmc.usp.br) da
[Universidade de São Paulo](https://usp.br) (ICMC/USP), que tem como objetivo
promover e apoiar atividades que envolvam a produção e/ou a disseminação de
artefatos Open Sources.

O CCOS oferece aconselhamento técnico e metodológico para usuários,
desenvolvedores e gestores de projetos Open Source, incluindo organizações e
indivíduos, quer sejam da academia, indústria, administração pública ou outros
segmentos da sociedade. 

Incluem-se no escopo da atuação do CCOS:

- **esclarecimentos** sobre direitos autorais, propriedade intelectual, seleção 
de licenças, procedimentos de publicação, metodologias e ferramentas de gestão,
dentre outros aspectos técnicos, legais e gerenciais;

- **suporte institucional** às atividades de ensino, pesquisa e extensão
universitária, bem como àquelas administrativas da Universidade, que envolvam a
geração e disseminação de tecnologias Open Source;

- **promoção de parcerias** entre Universidade e Indústria, bem como o
intercâmbio permanente com organizações promotoras de tecnologias Open Source e
a comunidade de usuários dessas tecnologias;

- **atividades de divulgação** e promoção do uso de sistemas operacionais e
ferramentas computacionais open source em escolas e instituições públicas da
região.

## O que é Open Source?

Qualificam-se como Open Source todas as formas de artefatos intelectuais abertos
que expressam conhecimento científico, tecnológico ou artístico em suas diversas
materializações, abarcando, mas não se limitando a:

- **software computacional**, incluindo código fonte, formatos de arquivos,
padrões de codificação de dados e projeto de interface;

- **projeto de hardware**, incluindo especificações, desenhos esquemáticos,
interface de operação e comunicação e desenho industrial;

- **obras literárias**, incluindo material didático, textos científicos, manuais
técnicos e imagens;

- **recursos multimídia**, incluindo arquivos de áudio, vídeo e conteúdo
interativo;

- **expressões artísticas**, incluindo arte gráfica, musical e plástica;

- **dados de pesquisa**, incluindo pacotes de  experimentação, resultados de
simulação, diagramas e imagens. 

A disponibilização pública desses artefatos garante condições equitativas e
irrestritas de utilização para quaisquer finalidades e em quaisquer condições,
modificação e distribuição na sua forma original ou modificada, gratuita e/ou
comercial, desde que estejam de acordo com a licença dos artefatos.

## Equipe do CCOS
{% include pessoa.html name="Elisa Yumi Nakagawa" lattes="K4790213Y9" email="elisa@icmc.usp.br" %}

{% include pessoa.html name="Francisco José Monaco" lattes="K4795439E1" email="monaco@icmc.usp.br" %}
